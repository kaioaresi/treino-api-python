# API com Flask Python

**Tips:** install extention

`Abra com o commando ⇧⌘X`

Estudar:

- [pep8](https://pep8.org/)
- [Type annotations](https://realpython.com/python-type-checking/)
- [Gerenciador de contexto](https://ichi.pro/pt/a-magia-dos-gerenciadores-de-contexto-python-191010798706128)
- [Penste EAFP - Easier to ask for forgiveness than permission.](https://docs.python.org/3/glossary.html)

# Pep 8

## Naming Styles

Type | Naming Convention | Examples
:---:|:---:|:---:
Function | Use a lowercase word or words. Separate words by underscores to improve readability.	| function, my_function
Variable | Use a lowercase single letter, word, or words. Separate words with underscores to improve readability. | x, var, my_variable
Class | Start each word with a capital letter. Do not separate words with underscores. This style is called camel case. | Model, MyClass
Method | Use a lowercase word or words. Separate words with underscores to improve readability. | class_method, method
Constant | Use an uppercase single letter, word, or words. Separate words with underscores to improve readability. | CONSTANT, MY_CONSTANT, MY_LONG_CONSTANT
Module | Use a short, lowercase word or words. Separate words with underscores to improve readability. | module.py, my_module.py
Package | Use a short, lowercase word or words. Do not separate words with underscores. | package, mypackage



---

# Referencias

- [SQLALCHEMY](https://flask-sqlalchemy.palletsprojects.com/en/2.x/config/)
- [Vs Code ext - ms-python.python](https://marketplace.visualstudio.com/items?itemName=ms-python.python)
- [Vs Code ext - dongli.python-preview](https://marketplace.visualstudio.com/items?itemName=dongli.python-preview)
- [Vs Code ext - njpwerner.autodocstring](https://marketplace.visualstudio.com/items?itemName=njpwerner.autodocstring)
- [Vs Code ext - vscode-phpunit](https://marketplace.visualstudio.com/items?itemName=recca0120.vscode-phpunit)
- [Real python pep8](https://realpython.com/python-pep8/)
- [Guide pep8 cheat sheet](https://cheatography.com/jmds/cheat-sheets/python-pep8-style-guide/)