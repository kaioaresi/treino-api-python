from flask import Flask, request

from main import create_user

app = Flask("Teste api python")


@app.route("/hello", methods=["GET"])
def hello():
    return {"msg": "Hello World"}


@app.route("/cadastro/user", methods=["POST"])
def cadastra_user():
    body = request.get_json()

    if "name" not in body:
        return gera_response(400, "Parametro 'name' não foi declarado!")

    if "password" not in body:
        return gera_response(400, "Parametro 'password' não foi declarado!")

    if "email" not in body:
        return gera_response(400, "Parametro 'email' não foi declarado!")

    user = create_user(body["name"], body["password"], body["email"])

    return gera_response(200, "Usuário criado!", "user", user)


def gera_response(status, mensagem, nome_do_conteudo=False, conteudo=False):
    response = {}
    response["Status"] = status
    response["mensagem"] = mensagem
    if nome_do_conteudo and conteudo:
        response[nome_do_conteudo] = conteudo
    return response


# Run
app.run()
