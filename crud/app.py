from flask import Flask, Response, request
from flask_sqlalchemy import SQLAlchemy
import mysql.connector
import json


app = Flask(__name__)
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = True
app.config["SQLALCHEMY_DATABASE_URI"] = "mysql://root:@127.0.0.1/crud"
db = SQLAlchemy(app)


class User(db.Model):
    """Criar as colunas da tabela"""

    id = db.Column(db.Integer, primary_key=True)
    nome = db.Column(db.String(50))
    email = db.Column(db.String(100))


@app.route("/users", methods=["GET"])
def select_all_users():
    """Realizar um select em toda tabela;

    Returns:
        responde: retorna todos dados da tabela
    """
    class_user = User.guery.all()
    print(class_user)
    return Response()


# Para criar a tabela ^
# db.create_all()

# 6:38

# TODO:
#  Select all
#  Select one
#  Create
#  Update
#  Deletar

app.run()
